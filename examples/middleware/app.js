'use strict';

const express = require( 'express' );
const { middlewareFactory, Engine } = require( '../../' );

// Cassandra config
const config = {
	hosts: [ '127.0.0.1' ],
	localDc: 'datacenter1',
	keyspace: 'tests',
	storageTable: 'storage',
	username: 'cassandra',
	password: 'cassandra'
};

// Function to define storage entry key
const keyFunc = ( req ) => req.originalUrl;
// Function to define storage entry project
const projectFunc = ( req ) => req.params.project;
// TTL for storage entries
const TTL = 0;

const app = express();
app.cache = new Engine( config );
app.get(
	'/:project/example',
	middlewareFactory( keyFunc, projectFunc, TTL ),
	( req, res ) => {
		res.send( 'Hello World!' );
	}
);

const port = 3000;
app.listen( port, () => {
	console.log( `Example app listening on port ${port}` );
} );
