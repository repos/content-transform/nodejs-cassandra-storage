# ExpressJS middleware example

## Setup

Prepare cassandra environment:

```
> cd $(git rev-parse --show-toplevel)
> docker compose run tests bash
> node
node REPL> const testHelpers = require("./tests/helpers")
node REPL> await testHelpers.setupCassandraTestEnv()
node REPL> .exit
```

Show detailed information of our test cassandra keyspace:

```
> cd $(git rev-parse --show-toplevel)
> docker compose exec -it cassandra cqlsh
cqlsh > DESCRIBE KEYSPACE tests;
```

Install local development dependencies:

```
> cd $(git rev-parse --show-toplevel)
> npm install
> cd examples/middleware
```

Start test nodejs app server:

```
node app.js
```

Send an HTTP GET request

```
curl 127.0.0.1:3000/test-project/example
```

List entries in cassandra:

```
> cd $(git rev-parse --show-toplevel)
> docker compose exec -it cassandra cqlsh
> SELECT * FROM tests.storage;
```
