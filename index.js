'use strict';

const Engine = require( './lib/engine' );
const middlewareFactory = require( './lib/middleware' );

module.exports = {
	Engine,
	middlewareFactory
};
