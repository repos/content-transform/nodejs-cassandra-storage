'use strict';
const cassandra = require( 'cassandra-driver' );
const config = require( './config' );

class CassandraEngine {
	/**
	 * @summary Create a new CassandraEngine object
	 * @description
	 * ```
	 * Expected  schema:
	 * CREATE TABLE <keyspace>.<name> (
	 *     project  text,
	 *     key      text,
	 *     headers  map<text,text>,
	 *     cached   timestamp,
	 *     value    blob,
	 *     PRIMARY KEY ((project, key))
	 * );
	 *```
	 * @param {config.Config} cfgObj
	 */
	constructor( cfgObj ) {
		this.client = new cassandra.Client( config.getClientOptions( cfgObj ) );
		this.defaultQueryOptions = {
			prepare: true,
			consistency: cassandra.types.consistencies.localQuorum
		};
		this.table = cfgObj.storageTable;
		this.setQuery = `UPDATE ${this.table} USING TTL ? SET value = ?, cached = ?, headers = ? WHERE key = ? AND project = ?`;
		this.getQuery = `SELECT * FROM ${this.table} WHERE key = ? AND project = ?`;
		this.deleteQuery = `DELETE FROM ${this.table} WHERE key = ? AND project = ?`;
	}

	/**
	 * Set a response to storage
	 *
	 * @param {string} key response key
	 * @param {string} project response project
	 * @param {Object.<string, string>} headers response headers
	 * @param {Buffer} value response body
	 * @param {number} ttl time to live in storage
	 */
	async set( key, project, headers, value, ttl = 0 ) {
		const cached = Date.now();
		const params = [ ttl, value, cached, headers, key, project ];
		await this.client.execute( this.setQuery, params, this.defaultQueryOptions );
	}

	/**
	 * Get a response from storage
	 *
	 * @param {string} key response key
	 * @param {string} project response project
	 * @return {Buffer} response value
	 */
	async get( key, project ) {
		const params = [ key, project ];
		const res = await this.client.execute( this.getQuery, params, this.defaultQueryOptions );
		const firstRes = res.first();

		if ( firstRes ) {
			return {
				headers: firstRes.headers,
				value: firstRes.value,
				cached: firstRes.cached
			};
		}
		return null;
	}

	/**
	 * Delete a response from storage
	 *
	 * @param {string} key response key
	 * @param {string} project response project
	 */
	async delete( key, project ) {
		const params = [ key, project ];
		await this.client.execute( this.deleteQuery, params, this.defaultQueryOptions );
	}

	/**
	 * Close CassandraEngine client connections
	 */
	async close() {
		await this.client.shutdown();
	}
}

module.exports = CassandraEngine;
