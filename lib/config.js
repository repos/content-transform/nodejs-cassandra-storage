'use strict';
const fs = require( 'fs' );
const cassandra = require( 'cassandra-driver' );
const loadBalancing = cassandra.policies.loadBalancing;

/**
 * @typedef {Object} Config
 * @property {string} keyspace - Cassandra keyspace
 * @property {string[]} hosts - Cassandra hosts
 * @property {number} port - Cassandra port
 * @property {boolean} [localDc] - Local datacenter name
 * @property {string} username - Plain-text authentication username
 * @property {string} password - Plain-text authentication password
 * @property {string} storageTable - Storage table
 * @property {{cert: string, key: string, ca: string[]}} [tls] - TLS configuration
 */

/**
 * Get cassandra client options
 *
 * @param {Config} config
 * @return {cassandra.ClientOptions} client options
 */
function getClientOptions( config ) {
	const options = {
		keyspace: config.keyspace,
		contactPoints: config.hosts,
		port: config.port,
		policies: {
			loadBalancing: config.localDc ?
				new loadBalancing.TokenAwarePolicy(
					new loadBalancing.DCAwareRoundRobinPolicy( config.localDc )
				) :
				loadBalancing.DefaultLoadBalancingPolicy()
		},
		authProvider: new cassandra.auth.PlainTextAuthProvider(
			config.username,
			config.password
		)
	};

	if ( config.tls ) {
		const sslOpts = {};

		if ( config.tls.cert ) {
			// eslint-disable-next-line security/detect-non-literal-fs-filename -- Trusted input from config
			sslOpts.cert = fs.readFileSync( config.tls.cert );
		}
		if ( config.tls.key ) {
			// eslint-disable-next-line security/detect-non-literal-fs-filename -- Trusted input from config
			sslOpts.key = fs.readFileSync( config.tls.key );
		}

		if ( config.tls.ca ) {
			// eslint-disable-next-line security/detect-non-literal-fs-filename -- Trusted input from config
			sslOpts.ca = config.tls.ca.map( ( ca ) => fs.readFileSync( ca ) );
		}
		options.sslOptions = sslOpts;
	}

	return options;
}

module.exports = {
	getClientOptions
};
