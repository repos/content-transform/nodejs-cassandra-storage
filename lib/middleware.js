'use strict';

/**
 * Check if request needs cache invalidation
 *
 * @param {Object} req Incoming request object
 * @return {boolean}
 */
const needsInvalidation = ( req ) => {
	return req.headers[ 'cache-control' ] === 'no-cache';
};

/**
 * Check if response is successful
 *
 * @param {Object} res Outgoing response object
 * @return {boolean}
 */
const isResponseSuccess = ( res ) => {
	return res.statusCode >= 200 && res.statusCode < 300;
};

/**
 * Response caching middleware factory
 *
 * @param {Function} keyFunc Function to define the entry key
 * @param {Function} projectFunc Function to define the entry project
 * @param {Function} updateHook Function to run after cache set
 * @param {Function} hitHook Function to run after cache hit
 * @param {number} ttl Time-to-live of the object in storage. ttl=0 means no TTL.
 * @param {number} maxJitter Max jitter time to spread object invalidation
 * @return {Function} express middleware for caching
 */
const cachedMiddlewareFactory = (
	keyFunc,
	projectFunc,
	updateHook = () => undefined,
	hitHook = () => undefined,
	ttl = 0,
	maxJitter = 0
) => {
	return async function ( req, res, next ) {
		const backend = req.app.cache;
		const key = keyFunc( req );
		const project = projectFunc( req );

		// Invalidate cache based on cache-control
		if ( needsInvalidation( req ) ) {
			await backend.delete( key, project );
		}

		// Query database for existing response
		const hitObj = await backend.get( key, project );
		if ( hitObj ) {
			// Cache hit
			hitHook( req, res, hitObj );

			// Set existing headers and body
			res.set( hitObj.headers );
			res.send( hitObj.value.toString() );
			return;
		}

		// Override send to allow storing responses
		res.sendRes = res.send;
		res.send = ( body ) => {
			if ( isResponseSuccess( res ) ) {
				const buf = Buffer.from( body );
				const headers = Object.fromEntries( Object.entries( res.getHeaders() ) );
				// Store headers/body for given key/project for TTL amount of seconds

				// If ttl is defined add some jitter else fallback to ttl
				const ttlWithJitter = ttl ? ttl + Math.floor( Math.random() * maxJitter ) : ttl;

				backend.set( key, project, headers, buf, ttlWithJitter )
					.then( () => updateHook( req, res ) )
					.catch( next );
			}

			return res.sendRes( body );
		};

		next();
	};
};

module.exports = cachedMiddlewareFactory;
