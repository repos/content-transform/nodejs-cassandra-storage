'use strict';
const cassandra = require( 'cassandra-driver' );

/**
 * Get a config object for testing purposes
 *
 * @return {Object}
 */
function getTestCassandraConfig() {
	const hosts = process.env.STORAGE_BACKEND_CASSANDRA_HOST || '["127.0.0.1"]';
	return {
		hosts: JSON.parse( hosts ),
		localDc: process.env.STORAGE_BACKEND_CASSANDRA_DC || 'datacenter1',
		keyspace: process.env.STORAGE_BACKEND_CASSANDRA_KEYSPACE || 'tests',
		storageTable: process.env.STORAGE_BACKEND_CASSANDRA_TABLE || 'storage',
		username: process.env.STORAGE_BACKEND_CASSANDRA_USER || 'cassandra',
		password: process.env.STORAGE_BACKEND_CASSANDRA_PASS || 'cassandra'
	};
}

/**
 * Setup cassandra keyspace and table for testing purposes
 */
async function setupCassandraTestEnv() {
	const config = getTestCassandraConfig();
	const keyspace = config.keyspace;
	const table = config.storageTable;
	const client = new cassandra.Client( {
		contactPoints: config.hosts,
		localDataCenter: config.localDc
	} );

	const replication = "{'class': 'SimpleStrategy', 'replication_factor': 1}";

	await client.execute( `CREATE KEYSPACE IF NOT EXISTS ${keyspace} WITH REPLICATION = ${replication}` );
	const fields = [
		'project text',
		'key text',
		'headers map<text, text>',
		'cached timestamp',
		'value blob',
		'PRIMARY KEY ((project, key))'
	];
	await client.execute( `CREATE TABLE ${keyspace}.${table} (${fields.join( ',' )})` );
	await client.shutdown();
}

/**
 * Tear down cassandra keyspace and table for testing purposes
 */
async function tearDownCassandraTestEnv() {
	const config = getTestCassandraConfig();
	const client = new cassandra.Client( {
		contactPoints: config.hosts,
		localDataCenter: config.localDc
	} );
	await client.execute( `DROP KEYSPACE IF EXISTS ${config.keyspace}` );
	await client.shutdown();
}

module.exports = {
	setupCassandraTestEnv,
	tearDownCassandraTestEnv,
	getTestCassandraConfig
};
