'use strict';
const { describe, before, it } = require( 'mocha' );
const fs = require( 'fs' );
const path = require( 'path' );
const os = require( 'os' );
const crypto = require( 'crypto' );
const config = require( '../lib/config' );
const assert = require( 'assert' );

const testConfigInput = {
	port: 9042,
	consistency: 'localQuorum',
	hosts: [
		'192.168.1.1',
		'192.168.1.2'
	],
	// eslint-disable-next-line camelcase
	local_dc: 'somedc',
	authentication: {
		username: 'cassandra',
		password: 'foobarbaz'
	},
	localDc: 'somedc',
	username: 'cassandra',
	password: 'foobarbaz',
	keyspace: 'tests',
	storageTable: 'storage'
};

describe( 'Config parsing helper', function () {
	let testCaPath;
	let testCertPath;
	let testKeyPath;

	before( function () {
		const prefix = 'test-nodejs-cassandra-storage';
		const testID = crypto.randomUUID();
		const dir = fs.mkdtempSync( path.join( os.tmpdir(), `${prefix}-${testID}` ) );
		testCaPath = path.join( dir, 'ca.crt' );
		testCertPath = path.join( dir, 'cert.crt' );
		testKeyPath = path.join( dir, 'cert.key' );
		// eslint-disable-next-line security/detect-non-literal-fs-filename
		fs.writeFileSync( testCaPath, 'example ca cert' );
		// eslint-disable-next-line security/detect-non-literal-fs-filename
		fs.writeFileSync( testCertPath, 'example cert' );
		// eslint-disable-next-line security/detect-non-literal-fs-filename
		fs.writeFileSync( testKeyPath, 'example cert key' );
	} );

	it( 'Should properly set basic options', function () {
		const cassandraCfgObj = config.getClientOptions( testConfigInput );
		assert.equal( cassandraCfgObj.keyspace, 'tests' );
		assert.equal( cassandraCfgObj.port, 9042 );
		assert.deepEqual( cassandraCfgObj.contactPoints, [ '192.168.1.1', '192.168.1.2' ] );
	} );

	it( 'Should properly set auth options', function () {
		const cassandraCfgObj = config.getClientOptions( testConfigInput );
		assert.equal( cassandraCfgObj.authProvider.constructor.name, 'PlainTextAuthProvider' );
		assert.equal( cassandraCfgObj.authProvider.username, 'cassandra' );
		assert.equal( cassandraCfgObj.authProvider.password, 'foobarbaz' );
	} );

	it( 'Should properly set auth options', function () {
		const cassandraCfgObj = config.getClientOptions( testConfigInput );
		assert.equal( cassandraCfgObj.policies.loadBalancing.constructor.name, 'TokenAwarePolicy' );
		assert.equal( cassandraCfgObj.policies.loadBalancing.childPolicy.constructor.name, 'DCAwareRoundRobinPolicy' );
		assert.equal( cassandraCfgObj.policies.loadBalancing.childPolicy.localDc, 'somedc' );
	} );

	it( 'Should properly set tls options', function () {
		const testConfig = Object.assign( {}, testConfigInput );
		testConfig.tls = {
			ca: [ testCaPath ],
			key: testKeyPath,
			cert: testCertPath
		};
		const cassandraCfgObj = config.getClientOptions( testConfig );
		assert.deepEqual( cassandraCfgObj.sslOptions, {
			ca: [ Buffer.from( 'example ca cert' ) ],
			key: Buffer.from( 'example cert key' ),
			cert: Buffer.from( 'example cert' )
		} );
	} );
} );
