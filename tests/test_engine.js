'use strict';
const { describe, beforeEach, afterEach, it } = require( 'mocha' );
const assert = require( 'assert' );
const testHelpers = require( './helpers' );
const CassandraEngine = require( '../lib/engine' );

describe( 'Cassandra engine tests', function () {
	beforeEach( async function () {
		await testHelpers.setupCassandraTestEnv();
	} );

	afterEach( async function () {
		await testHelpers.tearDownCassandraTestEnv();
	} );

	const queryAll = async ( engine, config ) => {
		return await engine.client.execute(
			`SELECT * FROM ${config.keyspace}.${config.storageTable}`
		);
	};

	it( 'Should initialize/shutdown engine', async function () {
		const config = testHelpers.getTestCassandraConfig();
		assert.doesNotThrow( async function () {
			const engine = new CassandraEngine( config );
			let state = engine.client.getState();
			assert.ok( state.getConnectedHosts().length === 0 );
			await engine.client.connect();
			state = engine.client.getState();
			assert.ok( state.getConnectedHosts().length > 0 );
			await engine.close();
			state = engine.client.getState();
			assert.ok( state.getConnectedHosts().length === 0 );
		} );
	} );

	describe( 'operations', function () {
		let config, engine;
		beforeEach( async function () {
			config = testHelpers.getTestCassandraConfig();
			engine = new CassandraEngine( config );
			assert.ok( ( await queryAll( engine, config ) ).rowLength === 0 );
		} );
		afterEach( async function () {
			engine.client.shutdown();
		} );

		describe( 'SET operation', function () {
			it( "compound key (key, project) doesn't exist - should add new row", async function () {
				await engine.set(
					'testkey',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' )
				);
				const after = await queryAll( engine, config );
				assert.ok( after.rowLength === 1 );
				assert.ok( after.first().key === 'testkey' );
				assert.ok( after.first().project === 'testproject' );
				assert.ok( Object.keys( after.first().headers ).length === 1 );
				assert.ok( after.first().headers.testheader === 'testheadervalue' );
				assert.ok( after.first().value.toString() === 'testbody' );
			} );

			it( 'compound key (key, project) exists - should update existing value', async function () {
				await engine.set(
					'testkey',
					'testproject',
					{ testheader1: 'testheader1-valuebefore' },
					Buffer.from( 'testbodybefore' )
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 1 );
				await engine.set(
					'testkey',
					'testproject',
					{ testheader1: 'testheader1-valueafter', testheader2: 'testheadervalue2' },
					Buffer.from( 'testbody2' )
				);
				const after = await queryAll( engine, config );
				assert.ok( after.rowLength === 1 );
				assert.ok( after.first().key === 'testkey' );
				assert.ok( after.first().project === 'testproject' );
				assert.ok( Object.keys( after.first().headers ).length === 2 );
				assert.ok( after.first().headers.testheader1 === 'testheader1-valueafter' );
				assert.ok( after.first().headers.testheader2 === 'testheadervalue2' );
				assert.ok( after.first().value.toString() === 'testbody2' );
			} );

			it( 'should add multiple new rows', async function () {
				await engine.set(
					'testkey1',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' )
				);
				await engine.set(
					'testkey2',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' )
				);
				await engine.set(
					'testkey3',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' )
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 3 );
			} );

			it( 'should add row with TTL (5s)', async function () {
				this.timeout( 10000 );
				await engine.set(
					'testkey',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' ),
					5
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 1 );
				await new Promise( function ( resolve ) {
					setTimeout( resolve, 5000 );
				} );
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 0 );
			} );
		} );

		describe( 'GET operation', function () {
			it( "compound key (key, project) doesn't exist - should return null", async function () {
				const obj = await engine.get( 'testkey', 'testproject' );
				assert.ok( obj === null );
			} );

			it( 'compound key (key, project) exists - should return existing row', async function () {
				await engine.set(
					'testkey',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' )
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 1 );
				const obj = await engine.get( 'testkey', 'testproject' );
				assert.ok( obj.headers.testheader === 'testheadervalue' );
				assert.ok( obj.value.toString() === 'testbody' );
			} );

			it( 'multiple rows exist - should return existing row', async function () {
				await engine.set(
					'testkey1',
					'testproject',
					{ testheader1: 'testheadervalue1' },
					Buffer.from( 'testbody1' )
				);
				await engine.set(
					'testkey2',
					'testproject',
					{ testheader2: 'testheadervalue2' },
					Buffer.from( 'testbody2' )
				);
				await engine.set(
					'testkey3',
					'testproject',
					{ testheader3: 'testheadervalue3' },
					Buffer.from( 'testbody3' )
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 3 );
				const obj = await engine.get( 'testkey1', 'testproject' );
				assert.ok( obj.headers.testheader1 === 'testheadervalue1' );
				assert.ok( obj.value.toString() === 'testbody1' );
			} );
		} );

		describe( 'DELETE operation', function () {
			it( "compound key (key, project) doesn't exist - should apply no changes", async function () {
				await engine.delete( 'testkey', 'testproject' );
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 0 );
			} );

			it( 'compound key (key, project) exists - should delete existing row', async function () {
				await engine.set(
					'testkey',
					'testproject',
					{ testheader: 'testheadervalue' },
					Buffer.from( 'testbody' )
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 1 );
				await engine.delete( 'testkey', 'testproject' );
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 0 );
			} );

			it( 'multiple rows exist - should delete existing row', async function () {
				await engine.set(
					'testkey1',
					'testproject',
					{ testheader1: 'testheadervalue1' },
					Buffer.from( 'testbody1' )
				);
				await engine.set(
					'testkey2',
					'testproject',
					{ testheader2: 'testheadervalue2' },
					Buffer.from( 'testbody2' )
				);
				await engine.set(
					'testkey3',
					'testproject',
					{ testheader3: 'testheadervalue3' },
					Buffer.from( 'testbody3' )
				);
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 3 );
				await engine.delete( 'testkey1', 'testproject' );
				assert.ok( ( await queryAll( engine, config ) ).rowLength === 2 );
				const after = await engine.get( 'testkey1', 'testproject' );
				assert.ok( after === null );
			} );
		} );
	} );
} );
