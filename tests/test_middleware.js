'use strict';

const { describe, beforeEach, afterEach, it } = require( 'mocha' );
const supertest = require( 'supertest' );
const assert = require( 'assert' );
const express = require( 'express' );
const sinon = require( 'sinon' );
const retry = require( 'async-retry' );
const testHelpers = require( './helpers' );
const cachedFactory = require( '../lib/middleware' );
const CassandraEngine = require( '../lib/engine' );

let app;
let errors = [];
const sandbox = sinon.createSandbox();
const TEST_ERR = 'Testing error handling';

const keyFunc = ( req ) => req.originalUrl;
const projectFunc = ( req ) => req.params.project;
const updateHook = sandbox.stub();
const hitHook = sandbox.stub();

const retryCacheGet = async ( cache, key, project ) => {
	return await retry( async () => {
		const res = await cache.get( key, project );
		if ( res === null ) {
			throw new Error( 'Storage entry not found' );
		}
		return res;
	}, { retries: 5 } );
};

const getCacheTTL = async ( cache, key, project ) => {
	const query = `SELECT TTL(value) as obj_ttl FROM ${cache.table} WHERE project = ? AND key = ?`;
	const params = [ project, key ];
	const res = await cache.client.execute( query, params );
	const firstRes = res.first();
	if ( firstRes ) {
		return firstRes.obj_ttl;
	}
	return null;
};

describe( 'Express middleware integration tests', function () {
	this.timeout( 10000 );
	const noCacheTTL = 0;
	const defaultCacheTTL = 60;
	const defaultCacheJitter = 60 * 5;

	beforeEach( async function () {
		errors = [];
		sandbox.restore();
		updateHook.reset();
		hitHook.reset();
		const config = testHelpers.getTestCassandraConfig();
		app = express();
		app.cache = new CassandraEngine( config );

		const captureErrors = ( err, req, res, next ) => {
			// Handle only mock errors
			if ( err.name === TEST_ERR ) {
				errors.push( err );
				return;
			}
			next( err );
		};

		app.get(
			'/:project/example',
			cachedFactory( keyFunc, projectFunc, updateHook, hitHook, noCacheTTL ),
			( req, res ) => {
				res.send( 'Hello World!' );
			}
		);
		app.get(
			'/:project/example-json',
			cachedFactory( keyFunc, projectFunc, updateHook, hitHook, noCacheTTL ),
			( req, res ) => {
				res.json( {
					'response-key1': 'response-value1',
					'response-key2': [ 'array-value1', 'array-value2' ]
				} );
			}
		);
		app.get(
			'/:project/custom-headers',
			cachedFactory( keyFunc, projectFunc, updateHook, hitHook, noCacheTTL ),
			( req, res ) => {
				res.set( 'x-custom-header-1', 'custom-value-1' );
				res.set( 'x-custom-header-2', 'custom-value-2' );
				res.send( 'Custom headers' );
			}
		);
		app.get(
			'/:project/redirect',
			cachedFactory( keyFunc, projectFunc, updateHook, hitHook, noCacheTTL ),
			( req, res ) => {
				res.redirect( 301, '/redirect-location' );
			}
		);
		app.get(
			'/:project/not-found',
			cachedFactory( keyFunc, projectFunc, updateHook, hitHook, noCacheTTL ),
			( req, res ) => {
				res.status( 404 ).send( 'Not found' );
			}
		);
		app.get(
			'/:project/test-ttl',
			cachedFactory( keyFunc,
				projectFunc,
				updateHook,
				hitHook,
				defaultCacheTTL
			),
			( req, res ) => {
				res.send( 'Hello world with ttl!' );
			}
		);

		app.get(
			'/:project/test-ttl-jitter',
			cachedFactory( keyFunc,
				projectFunc,
				updateHook,
				hitHook,
				defaultCacheTTL,
				defaultCacheJitter ),
			( req, res ) => {
				res.send( 'Hello world with ttl and jitter!' );
			}
		);

		app.use( captureErrors );

		await testHelpers.setupCassandraTestEnv();
	} );

	afterEach( async function () {
		await app.cache.close();
		await testHelpers.tearDownCassandraTestEnv();
	} );

	it( 'Should cache new response', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );

		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		const ttl = await getCacheTTL( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello World!' );
		assert( ttl === null );
	} );

	it( 'Should cache new response with TTL', async function () {
		const testURL = '/foo/test-ttl';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello world with ttl!' );

		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		const ttl = await getCacheTTL( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello world with ttl!' );
		assert( ttl <= defaultCacheTTL );
	} );

	it( 'Should cache new response with jitter in TTL', async function () {
		const testURL = '/foo/test-ttl-jitter';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello world with ttl and jitter!' );

		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		const ttl = await getCacheTTL( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello world with ttl and jitter!' );
		assert( ttl >= defaultCacheTTL && ttl <= defaultCacheTTL + defaultCacheJitter );
	} );

	it( 'Should call hit hook for storage hit', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello World!' );
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		sandbox.assert.calledOnce( hitHook );
	} );

	it( 'Should not call hit hook for storage miss', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello World!' );
		sandbox.assert.notCalled( hitHook );
	} );

	it( 'Should call update hook for storage misses', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello World!' );
		sandbox.assert.calledOnce( updateHook );
	} );

	it( 'Should not call update hook for storage hits', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );

		// Ensure that we have stored content
		assert( cacheObj.value.toString() === 'Hello World!' );

		updateHook.reset();
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		sandbox.assert.notCalled( updateHook );
	} );

	it( 'Should cache new JSON response', async function () {
		const testURL = '/foo/example-json';
		const testProject = 'foo';
		const expectedBody = {
			'response-key1': 'response-value1',
			'response-key2': [ 'array-value1', 'array-value2' ]
		};

		await supertest( app ).get( testURL ).expect( 200, expectedBody );

		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === JSON.stringify( expectedBody ) );
	} );

	it( 'Should return cached response', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );

		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello World!' );

		// Override existing value in cassandra
		const overridenResponse = 'Overriden cached value';
		await app.cache.set(
			testURL,
			testProject,
			{},
			Buffer.from( overridenResponse )
		);

		await supertest( app ).get( testURL ).expect( 200, overridenResponse );
	} );

	it( 'Should return cached JSON response', async function () {
		const testURL = '/foo/example-json';
		const testProject = 'foo';
		const expectedBody = {
			'response-key1': 'response-value1',
			'response-key2': [ 'array-value1', 'array-value2' ]
		};
		await supertest( app ).get( testURL ).expect( 200, expectedBody );
		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === JSON.stringify( expectedBody ) );

		// Override existing value in cassandra
		// Ensure that cached content-type is application/json
		const overridenJSON = {
			'overriden-key1': 'overriden-value1',
			'overriden-key2': [ 1, 2, 3 ]
		};
		const overridenResponse = JSON.stringify( overridenJSON );
		await app.cache.set(
			testURL,
			testProject,
			{ 'content-type': 'application/json' },
			Buffer.from( overridenResponse )
		);

		await supertest( app ).get( testURL ).expect( 200, overridenJSON );
	} );

	it( 'Should not return cached response for 30x status codes', async function () {
		const testURL = '/foo/redirect';
		const testProject = 'foo';
		await supertest( app )
			.get( testURL )
			.expect( 301 )
			.expect( 'Location', '/redirect-location' );
		const cacheObj = await app.cache.get( testURL, testProject );
		assert( cacheObj === null );
	} );

	it( 'Should not return cached response for 40x status codes', async function () {
		const testURL = '/foo/not-found';
		const testProject = 'foo';
		await supertest( app ).get( testURL ).expect( 404 );
		const cacheObj = await app.cache.get( testURL, testProject );
		assert( cacheObj === null );
	} );

	it( 'Should return cached headers', async function () {
		const testURL = '/foo/custom-headers';
		const testProject = 'foo';

		// Override existing value in cassandra
		await app.cache.set(
			testURL,
			testProject,
			{ 'overriden-header-1': 'overriden-value-1' },
			Buffer.from( 'Overriden body' )
		);

		const res = await supertest( app )
			.get( testURL )
			.expect( 200, 'Overriden body' )
			.expect( 'overriden-header-1', 'overriden-value-1' );

		assert( !Object.keys( res.headers ).includes( 'x-custom-header-1' ) );
		assert( !Object.keys( res.headers ).includes( 'x-custom-header-2' ) );
	} );

	it( 'Should invalidate storage content for a stored response', async function () {
		const testURL = '/foo/example';
		const testProject = 'foo';
		await supertest( app )
			.get( testURL )
			.expect( 200, 'Hello World!' );
		const cacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( cacheObj.value.toString() === 'Hello World!' );

		// Override existing value in cassandra
		await app.cache.set(
			testURL,
			testProject,
			{ 'overriden-header-1': 'overriden-value-1' },
			Buffer.from( 'Overriden body' )
		);
		const overridenCacheObj = await app.cache.get( testURL, testProject );
		assert( overridenCacheObj.value.toString() === 'Overriden body' );

		// Ensure that overriden response body is served
		await supertest( app )
			.get( testURL )
			.expect( 200, 'Overriden body' )
			.expect( 'overriden-header-1', 'overriden-value-1' );

		// Send invalidation request
		await supertest( app )
			.get( testURL )
			.set( 'cache-control', 'no-cache' )
			.expect( 200, 'Hello World!' );

		const invalidatedCacheObj = await retryCacheGet( app.cache, testURL, testProject );
		assert( invalidatedCacheObj.value.toString() === 'Hello World!' );
	} );

	it( 'Should throw error on cache SET and return 200', async function () {
		const testURL = '/foo/example';
		const errMsg = TEST_ERR;
		app.cache.set = sinon.stub().rejects( errMsg );
		await supertest( app ).get( testURL ).expect( 200, 'Hello World!' );
		assert.equal( errors.length, 1 );
		assert.equal( errors[ 0 ].name, errMsg );
	} );
} );
